using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class SaveGameManager : MonoBehaviour
{
    public static SaveGameManager Instance;

    public string playerName = "";

    public string bestPlayerName = "";

    public int bestScore = 0;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code

        Instance = this;
        DontDestroyOnLoad(gameObject);

        LoadGame();
    }

    public void SaveGame()
    {
        SaveData data = new SaveData();
        data.playerName = this.playerName;
        data.bestScore = this.bestScore;
        data.bestPlayerName = this.bestPlayerName;

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }

    public void LoadGame()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);

            this.playerName = data.playerName;
            this.bestScore = data.bestScore;
            this.bestPlayerName = data.bestPlayerName;
        }
    }

    [System.Serializable]
    class SaveData
    {
        public string playerName;

        public string bestPlayerName;

        public int bestScore;
    }

}
