using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public TextMeshProUGUI bestScoreText;
    public TMP_InputField playerNameInput;
    // Start is called before the first frame update
    void Start()
    {
        this.playerNameInput.SetTextWithoutNotify(SaveGameManager.Instance.playerName);
        this.bestScoreText.text = "Best Score " + SaveGameManager.Instance.bestScore + " from " + SaveGameManager.Instance.bestPlayerName;
    }

    public void StartGame()
    {
        SaveGameManager.Instance.playerName = this.playerNameInput.text;
        SceneManager.LoadScene("main");
    }

    public void EndGame()
    {
        SaveGameManager.Instance.SaveGame();

#if UNITY_EDITOR
    EditorApplication.ExitPlaymode();
#else
    Application.Quit();
#endif
    }
}
